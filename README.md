# README #

### Kibana Dashboard ###

Kibana dashboards and visualizations can be generated using the template provided in the repository. There is a python script which can be used to generate ndjson file in folder specific to the microproduct and environment. This script will also execute a curl command which will use the generated ndjson file and import the dashboard and visulaziations to the Kibana. The config file holds the url and the index details for each environment. The ndjson file can be used to import the objects from the Kibana UI also.

### How do I get set up? ###

* This project requires python3
* This has a dependency with ndjson. Install ndjson using the command `pip3 install ndjson`

### How to Run the script ###

Run the script using `python3 CreateDashboard.py --env=<environment-name> --project=<project-name>`

Example: `python3 CreateDashboard.py --env=stage3 --project=suite`
