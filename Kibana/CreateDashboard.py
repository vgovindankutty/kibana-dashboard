import os
import ndjson
import sys
import json
import getopt
import requests

with open('./base/template.ndjson') as f:
    data = ndjson.load(f)
    text = ndjson.dumps(data)

try:
    opts, args = getopt.getopt(sys.argv[1:], "p:e", ["project=", "env="])
except getopt.GetoptError as e:
    print('Failed to parse opts:')
    print(e)
    sys.exit(2)

project, environment = "",""

for opt, arg in opts:
    if opt in ("-p", "--project"):
        if arg.startswith('aero'):
            project = arg
        else:
            project = f"aero-{arg}"
    elif opt in ("-e", "--env"):
        environment = arg

with open('config/config.json') as f:
    data = json.load(f)
indexId = data[environment]["indexId"]
kibanaurl = data[environment]["url"]

text = text.replace('kibana_namespace',project+'-'+environment)
text = text.replace('kibana_title_1',environment.capitalize()+'-'+project+'-Proxy Statistics')
text = text.replace('kibana_title_2',environment.capitalize()+'-'+project+'-Proxy Response Codes')
text = text.replace('kibana_title_3',environment.capitalize()+'-'+project+'-Proxy Hit Rates')
text = text.replace('kibana_title_4',environment.capitalize()+'-'+project+'-Proxy Response Times')
text = text.replace('kibana_title_5',environment.capitalize()+'-'+project+'-Dashboard')
text = text.replace('kibana_index_id',indexId)
text = text.replace('kibana_vis_id_1','kibana_vis_id_1_'+project+'_'+environment)
text = text.replace('kibana_vis_id_2','kibana_vis_id_2_'+project+'_'+environment)
text = text.replace('kibana_vis_id_3','kibana_vis_id_3_'+project+'_'+environment)
text = text.replace('kibana_vis_id_4','kibana_vis_id_4_'+project+'_'+environment)
text = text.replace('kibana_dash_id','kibana_dash_id_'+project+'_'+environment)
text = text.replace('kibana_ip_address',kibanaurl)

data = ndjson.loads(text)

filename = "output/"+project+"/"+environment+"/import.ndjson"
os.makedirs(os.path.dirname(filename), exist_ok=True)

with open(filename, 'w') as f:
    ndjson.dump(data, f)

os.system("curl -X POST 'http://172.31.18.2:5601/api/saved_objects/_import' -H 'kbn-xsrf: true' --form file=@"+filename)